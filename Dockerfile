ARG FLUENTD_VERSION=v1.16.2-debian-elasticsearch7-1.0

FROM fluent/fluentd-kubernetes-daemonset:${FLUENTD_VERSION}

RUN apt update && \
    apt install -y make gcc && \
    fluent-gem install fluent-plugin-mongo && \
    apt remove -y make gcc && \
    apt autoremove -y && \
    apt clean
